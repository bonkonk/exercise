#!/bin/python

## init ##
print("hi, i'm staring now Oo")
array = [1,2,3,4]

## globals ##

# global cnt to count accross recursion
cnt = 0
def count():
    global cnt
    cnt = cnt+1


## FXS ##    
def permutate(a, k):
    # exit at last element
    if k+1 >= len(a):
        count()
        # unique permutation can be found here
        print("==>",a)
        return

    i = k
    while i < len(a):
        # make deep copy of array
        b=a.copy()

        # swap elements
        b[i], b[k] = a[k], a[i]

        # go recursive
        permutate(b, k+1)
        
        i = i+1
        

## start ##
permutate(array,0)

print(cnt," permutations calculated")

